<html lang="en">

<head>
    <title>Sorter</title>
    <link rel="stylesheet" href="Style/styl.css" type="text/css" />
</head>

<body>

    <?php
    function print_arr($arr){
        foreach ($arr as $key=>$item){
            echo "$item ";
        }
    }


    function qsort(&$array) {
        $left = 0;
        $right = count($array) - 1;

        quick_sort($array, $left, $right);

    }


    function quick_sort(&$array, $left, $right) {
        $l = $left;
        $r = $right;

        $center = $array[(int)($left + $right) / 2];

        do {
            while ($array[$r] > $center) {
                $r--;
            }

            while ($array[$l] < $center) {
                $l++;
            }

            if ($l <= $r) {

                list($array[$r], $array[$l]) = array($array[$l], $array[$r]);

                $l++;
                $r--;
            }

        } while ($l <= $r);

        if ($r > $left) {
            quick_sort($array, $left, $r);
        }

        if ($l < $right) {
            quick_sort($array, $l, $right);
        }
    }

        $array = explode(',', $_SERVER['QUERY_STRING']);
        
        qsort($array);
        print_arr($array);
    ?>


</body>

</html>